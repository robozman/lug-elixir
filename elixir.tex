\documentclass{lug}

\usepackage{fontawesome}
\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}
\usepackage{attrib}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\newcommand{\make}{GNU \texttt{make}\xspace}

\title{The Elixir Programming Language}
\author{Robby Zampino}
\institute{Mines Linux Users Group}

\begin{document}

\section{History of Elixir}

\begin{frame}{Erlang}

  \pause
  In the 1980s, engineers an Ericsson developed a prototype of the Erlang
  programming language. This language was designed for building telephone
  exchanges.

  \pause
  The original version of Erlang was implemented using a interpreter
  written in Prolog. This proved too slow for doing anything except
  prototyping.

  \pause
  In 1992, work began on the BEAM virtual machine. The BEAM virtual machine
  ``compiles Erlang to C using a mix of natively compiled code and threaded
  code to strike a balance between performance and disk space.''

 
\end{frame}

\begin{frame}{Erlang Principles}

  \pause
  Erlang was built based around the idea of ``Processes''.

  \pause
  ``Joe Armstrong, co-inventor of Erlang, summarized the
  principles of processes in his PhD thesis:''

  \pause
  \begin{itemize}
  \item Everything is a process
    \pause
  \item Processes are strongly isolated
    \pause
  \item Process creation and destruction is a lightweight operation.
    \pause
  \item Message passing is the only way for processes to interact.
    \pause
  \item Processes have unique names.
    \pause
  \item If you know the name of a process you can send it a message.
    \pause
  \item Processes share no resources.
    \pause
  \item Error handling is non-local.
    \pause
  \item Processes do what they are supposed to do or fail.
    \pause
  \end{itemize}
  
\end{frame}

\begin{frame}{Ruby}

  \pause
  José Valim is the creator of the Elixir programming language

  \pause
  His goals were to enable higher extensibility and productivity
  in the Erlang VM while keeping compatibility with Erlang's
  ecosystem.
  
  \pause
  Being a Ruby developer, he used the best features of Ruby,
  Erlang, and Clojure to develop a high-concurrency and
  low-latency language.
  
\end{frame}

\section{Elixir}

\begin{frame}{Elixir}

  \pause
  Elixir aims to combine the best of Erlang's ecosystem with the
  syntax and build tools of a modern functional programming language.

  \pause
  Elixir, like many other functional langugaes, treats every variable
  as immutable.
\end{frame}


\begin{frame}{Elixir Interpreter}
  \pause
  But I thought elixir was compiled?

  \pause
  Well......kinda?

  \pause
  Elixir provides an interpreter, \texttt{iex}. \texttt{iex} allows
  you to run elixir code and expressions interactivly for development
  purposes.

  \pause
  When you want to run Elixir code outside of the interpreter,
  it can be compiled and packaged alongside the BEAM VM easily.
  
\end{frame}

\section{Syntax}

\begin{frame}[fragile]{Transformations}
  \pause
  Applying multiple transformations to data is something that is
  frequently done. An example of this could be converting as string
  to uppercase and then splitting on spaces

  \pause
  \begin{minted}{elixir}
    iex(1)> x = "help me"
    iex(2)> y = String.upcase(x)
    iex(3)> z = String.split(x)
    iex(4)> z
    ["HELP", "ME"]
  \end{minted}

  \pause
  This syntax can be tedious, especially when the amount of
  transformations grows.
  
\end{frame}

\begin{frame}[fragile]{Pipe Operator}
  \pause
  To symplify this syntax, Elixir uses the pipe operator.

  \pause
  \begin{minted}{elixir}
    iex(1)> x = "help me" |> String.upcase |> String.split
    iex(2)> x
    ["HELP", "ME"]
  \end{minted}

\end{frame}

\begin{frame}[fragile]{The Pipe Operator and HTTP Responses}
  
  \pause
  The pipe operator isn't unique to Elixir, other programming languaes
  have one that functions similarly. The reason why the pipe operator
  is uniquely useful in Elixir ties back to it's use in packet parsing
  and routing.

  \pause
  Another use case is in responding to an http request.
  Sometimes there are mutiple operatations you want to
  do on a socket, such as setting headers and then
  sending the actual requested file.

\end{frame}

\begin{frame}[fragile]{The Pipe Operator and HTTP Responses}

  \pause
  Examples:
  
  \pause
  \begin{minted}{elixir}
    def new(conn, %{"token" => token}) do
      user = Resource.find_user_by_token(token)conn
        |> assign(:token, token)
        |> assign(:email, Map.get(user, :email))
        |> render("new.html")
      end
  \end{minted}

  \pause
  \begin{minted}{elixir}
    conn
      |> put_resp_content_type("audio/mpeg")
      |> send_file(200, output_file)
  \end{minted}

\end{frame}


\begin{frame}{Pattern Matching}
  \pause
  Another frequently used feature is Elixir is pattern
  matching. Elixir supports pattern matching on binary,
  string, atom and many other data types.

  \pause
  Example: Configuration file operations.
\end{frame}

\begin{frame}[fragile]{Pattern Matching}

  \pause
  \begin{minted}{elixir}
    case File.stat(config_file) do
      {:ok, _stat} ->
        Logger.info("Loading configuration")
        Logger.info("Config file loaded sucessfully")
        config

      {:error, :enoent} ->
        case File.stat(config_dir) do
          {:error, :enoent} ->
            Logger.info("Creating configuration folder")

          {:error, error} ->
            raise error

          {:ok, _stat} ->
            nil
        end

        Logger.info("Creating default config file")
        Logger.info("Loading configuration")
        Logger.info("Config file  loaded sucessfully")
        config
    end

  \end{minted}
\end{frame}


\begin{frame}[fragile]{UDP Parsing with Pattern Matching}
  \pause
  \begin{minted}{elixir}
    def parse_packet(data) do
      <<
        _header        :: size(240), # 30 bytes * 8 = 240 bits
        priority_code  :: bitstring-size(8),
        agent_number   :: little-unsigned-integer-size(32),
        message        :: bitstring-size(320)
      >> = data

      message = %{
        priority_code: priority_code,
        agent_number: agent_number,
        message: String.rstrip(message),
      }
    end
  \end{minted}
\end{frame}

\begin{frame}{Want to learn more?}
  Want to learn more?
  
 \begin{itemize}
    \pause\item Ask me! I like Elixir and have written quite at bit of it at this point
    \pause\item Tutorial: https://elixir-lang.org/getting-started/introduction.html
    \pause\item Elixir Package Manager: https://hex.pm/
  \end{itemize}
\end{frame}


\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
