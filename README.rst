Open Standards
##############
A Presentation About The Elixir Programming Language for LUG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Authors/Presenters
------------------

- `Robby Zampino`_

.. _Robby Zampino: https://gitlab.com/robozman

Topics Covered
--------------

- History of Elixir

  - Erlang

  - Ruby

- Intro to Elixir 

  - Pipe Operator

  - Pattern Matching

- Want to learn more?

